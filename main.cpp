#include <iostream>
#include <limits>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/centroid.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>
#include <pcl/filters/filter.h>
#include <pcl/conversions.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>


using namespace std;


class BoundingBoxProjector
{
	private:
		int state; // Controls the app flow
		vector<int> listOfBB; // BB1X1 BB1Y1 BB1X2 BB1Y2 Stores the BB points in the image plane
		vector<int> pointInFloor; // FLOORX FLOORY Stores the floor point in the image plane
		cv::Mat img;
		pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud;

	public:

		BoundingBoxProjector()
		{
		}

		/*
			open cv windows callback. It stores the X and Y positions when u click on the windows.
			First it stores the bounding box of the object, then a point in the floor
		*/
		static void CallBackFunc(int event, int x, int y, int flags, void* userdata)
		{
			BoundingBoxProjector* pointer = (BoundingBoxProjector*)userdata;

			if( event == 1 && pointer->state == 0) // Left mouse button
			{
				//cout << "Position (" << x << ", " << y << ")" << endl;
				pointer->listOfBB.push_back(x);
				pointer->listOfBB.push_back(y);
				/*if (pointer->listOfBB.size() % 4 == 0)
				{
					//cout << "BB set" << endl;
					pointer->state = 1;
				}*/
			}
			else if( event == 1 && pointer->state == 1) // Left mouse button
			{
				pointer->pointInFloor.push_back(x);
				pointer->pointInFloor.push_back(y); 
				pointer->state = 2;
			}
			else if ( event == 2)
			{
				pointer->state = 1;
			}
		}

		/*
			Performs the projection of the points inside the BB on the floor plane
		*/
		pcl::ModelCoefficients::Ptr process(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, cv::Mat img, bool verbose, pcl::ModelCoefficients::Ptr coefficients_out, vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr > &cloud_proj_flor_plane_out) {
			
			this->cloud = cloud;
			this->img = img;
			state = 0;
			listOfBB.clear();
			pointInFloor.clear();

			// Select the BB and the floor points in the opencv window

			//cout << "Select the bounding box of the object" << endl;
			cv::namedWindow("Select BB", 1);
			cv::setMouseCallback("Select BB", CallBackFunc, this);
			cv::imshow("Select BB", img);
			while (state < 2)
			{
				cv::waitKey(10);
			}
			cv::destroyAllWindows();


			// Extract the 3D points in the BB. We can do this straighforward as the pointcloud is registered to the color image

			int bboxes = listOfBB.size() / 4;
			cout << "n objects: " << bboxes << endl;


			// Find the fllor plane by fitting all dominant planes and stopping when a certain plain contains
			// the previously selected floor point

			pcl::PointXYZRGBA pointInFloorPC = cloud->points[(640 * pointInFloor[1] + pointInFloor[0])];

			pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_copy (new pcl::PointCloud<pcl::PointXYZRGBA>);
			pcl::copyPointCloud(*cloud, *cloud_copy);

			pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
			pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
			pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
			pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
			bool sigue = true;

			while(sigue){

				seg.setOptimizeCoefficients (true);
				seg.setModelType (pcl::SACMODEL_PLANE);
				seg.setMethodType (pcl::SAC_RANSAC);
				seg.setDistanceThreshold (0.05);
				seg.setInputCloud (cloud_copy);
				seg.segment (*inliers, *coefficients);

			    extract.setInputCloud (cloud_copy);    // Already done line 50
			    extract.setIndices (inliers);     // Already done line 51
			    extract.setNegative (true);       // Extract the outliers
			    extract.filter (*cloud_copy);   // cloud_outliers contains everything but the plane

			    sigue = false;
			    for(int i = 0; i < cloud_copy->points.size(); i++)
			    {
			    	if(cloud_copy->points[i].x == pointInFloorPC.x && cloud_copy->points[i].y == pointInFloorPC.y && cloud_copy->points[i].z == pointInFloorPC.z)
			    	{
			    		sigue = true;
			    		break;
			    	}
			    }	    
			}
			//cout << *coefficients << endl;
			*coefficients_out = *coefficients;


			// Iterate all the bounding boxes
			vector<pcl::PointXYZ> centroids;
			vector<float> radiuses;
			for(int k = 0; k < bboxes; k++)
			{
				pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_pda (new pcl::PointCloud<pcl::PointXYZRGBA>);

				for(int i = listOfBB[(k*4)+0]; i < listOfBB[(k*4)+2]; i++)
				{
					for(int j = listOfBB[(k*4)+1]; j < listOfBB[(k*4)+3]; j++)
					{
						int vecpos = 640 * j + i;
						pcl::PointXYZRGBA p = cloud->points[vecpos];
						cloud_pda->points.push_back(p);
					}
				}
				//cout << "size " <<  cloud_pda->points.size() << endl;
				
				// Compute the centroid of the points insede the bounding box
				pcl::PointXYZ centroid(0,0,0);
				int pointsSize = 0;
				for(int i = 0; i < cloud_pda->points.size(); i++)
				{
					if ((boost::math::isnan) (cloud_pda->points[i].x) ||
			            (boost::math::isnan) (cloud_pda->points[i].y) ||
			            (boost::math::isnan) (cloud_pda->points[i].z))
			          		continue;
			        pointsSize++;
					centroid.x += cloud_pda->points[i].x;
					centroid.y += cloud_pda->points[i].y;
					centroid.z += cloud_pda->points[i].z;

				}
				centroid.x /= float(pointsSize);
				centroid.y /= float(pointsSize);
				centroid.z /= float(pointsSize);
				centroids.push_back(centroid);
				//cout << "centroid: " << centroid << endl;


				// Compute a suitable radius for the sphere (only for visualziation purposes)
				pcl::PointXYZRGBA minPt, maxPt;
				pcl::getMinMax3D (*cloud_pda, minPt, maxPt);

				float radius = 0.0;
				vector<float> diffs;
				diffs.push_back((maxPt.x - minPt.x));
				diffs.push_back((maxPt.y - minPt.y));
				diffs.push_back((maxPt.z - minPt.z));
				for(int i = 0; i < diffs.size(); i++)
				{
					if (diffs[i] > radius)
					{
						radius = diffs[i];
					}
				}
				radius /= float(2.0);
				radius *= 1.7;
				radiuses.push_back(radius);
				//cout << "radius: " << radius << endl;


				// Finally project the 3D points of the bounding box to the previously extracted floor plane
				pcl::PointCloud<pcl::PointXYZRGBA>::Ptr output (new pcl::PointCloud<pcl::PointXYZRGBA>);
				pcl::ProjectInliers<pcl::PointXYZRGBA> proj;
				proj.setModelType (pcl::SACMODEL_PLANE);
				proj.setInputCloud (cloud_pda);
				proj.setModelCoefficients (coefficients);
				proj.filter (*output);

				// Return the projected points
				pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_proj_flor_plane (new pcl::PointCloud<pcl::PointXYZRGBA>);
				*cloud_proj_flor_plane = * output;
				cloud_proj_flor_plane_out.push_back(cloud_proj_flor_plane);
			}

			// Show results if the verbose mode is activated
			if(verbose){
				boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer ("3D Viewer"));
				//viewer->addCoordinateSystem(0.2);
				viewer->setBackgroundColor(0,0,0);
				viewer->initCameraParameters();
				viewer->addPointCloud<pcl::PointXYZRGBA>(cloud, "cloud");


				for(int i = 0; i < cloud_proj_flor_plane_out.size(); i++)
				{
					stringstream ss;
					ss << i;

					pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBA> single_color(cloud_proj_flor_plane_out[i], 0, 255, 0);
					viewer->addPointCloud<pcl::PointXYZRGBA>(cloud_proj_flor_plane_out[i], single_color, "pda_proj"+ss.str());

					viewer->addSphere(centroids[i], radiuses[i], 255,0,0, "sphere"+ss.str());
					viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.4, "sphere"+ss.str());
				}

				while(!viewer->wasStopped())
				{
					viewer->spinOnce(33);
				}
			}
		}
	
};

int main(int argc, char *argv[])
{
	char* imagePath = argv[1];
	char* pointPath = argv[2];

	// read an image and the corresponding point cloud
	string inputCloud = pointPath;
	string inputImage = imagePath;
	cout << inputCloud << endl;
	cout << inputImage << endl;
	bool verbose = true; // I want to display the results

	// input parameters: the image and the corresponding pointcloud
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
	pcl::io::loadPCDFile<pcl::PointXYZRGBA> (inputCloud, *cloud);
	cv::Mat img = cv::imread(inputImage);

	// Create the object
	BoundingBoxProjector bboxproj;
	// output parameters: coefficients and the projected points of the BB in the floor plane
	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
	vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr > cloud_proj_flor_plane;// (new pcl::PointCloud<pcl::PointXYZRGBA>);
	bboxproj.process(cloud, img, verbose, coefficients, cloud_proj_flor_plane);
	cout << *coefficients << endl;
	for(int i = 0; i < cloud_proj_flor_plane.size(); i++)
	{
		cout << cloud_proj_flor_plane[i]->points.size() << endl;
	}
}